import os
import logging
import configparser
import uvicorn
import sys
from fastapi import FastAPI
from fastapi.openapi.docs import get_redoc_html, get_swagger_ui_html
from fastapi.staticfiles import StaticFiles

# FastAPI App Setup
config = configparser.ConfigParser()
config.read("config.ini")  

# Read base_path from the configuration
base_path = config.get("API", "base_path", fallback="")  # Default to empty if not found

# FastAPI App Setup
app = FastAPI(
    title="Federation Manager API",
    description="API for managing hosts, tools, and more.",
    version="1.0",
    root_path=base_path,
    security=[{"OAuth2PasswordBearer": []}],
)

# Enable Swagger UI
app.add_route(
    f"{base_path}/docs",
    get_swagger_ui_html(openapi_url=f"{base_path}/openapi.json", title="API Documentation"),
)
app.add_route(
    f"{base_path}/redoc",
    get_redoc_html(openapi_url=f"{base_path}/openapi.json", title="API Documentation"),
)

# Serve Swagger UI and ReDoc static files
app.mount(f"{base_path}/docs/static", StaticFiles(directory="static"), name="docs_static")



# Configuration
HOST = os.getenv("HOST", "0.0.0.0")
PORT = int(os.getenv("PORT", 5000))
LOG_LEVEL = os.getenv("LOG_LEVEL", "info").upper()

# Set up logging
logging.basicConfig(
    filename=config.get("logging", "logfile", fallback='server.log'),
    level=getattr(logging, LOG_LEVEL, logging.INFO),
    format='%(asctime)s - %(levelname)s: %(message)s'
)

logging.info("Configuration loaded from %s", "config.ini")

# =================================================================================================
# API Endpoints
## Include routers with OAuth dependencies
from routers import tools, token, tasks, hosts, executions, data

# Uncomment next 2 lines to have the DDBBB connection test endpoint
# from routers import db_test
# app.include_router(db_test.router)
app.include_router(token.router, prefix="/token", tags=["Authentication"])
app.include_router(hosts.router, prefix="/hosts", tags=["Hosts"])
app.include_router(tools.router, prefix="/tools", tags=["Tools"])
app.include_router(tasks.router, prefix="/tasks", tags=["Tasks"])
app.include_router(data.router, prefix="/data", tags=["Data"])
app.include_router(executions.router, prefix="/executions", tags=["Executions"])

# Custom log config for uvicorn
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "%(asctime)s - %(levelname)s: %(message)s",
        },
        "access": {
            "format": '%(asctime)s - %(levelname)s - %(client_addr)s - "%(request_line)s" - %(status_code)s',
        },
    },
    "handlers": {
        "default": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "default",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "default",
            "filename": config.get("logging", "logfile", fallback="server.log"),
        },
        "uvicorn.access": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "access",
        },
        # Custom handler for stdout
        "stdout": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "default",
            "stream": sys.stdout,
        },
        # Custom handler for stderr
        "stderr": {
            "level": "ERROR",
            "class": "logging.StreamHandler",
            "formatter": "default",
            "stream": sys.stderr,
        },
    },
    "loggers": {
        "": {  # Root logger
            "handlers": ["default", "file", "stdout", "stderr"],
            "level": LOG_LEVEL,
        },
        "uvicorn": {
            "handlers": ["default", "file", "stdout", "stderr"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "uvicorn.error": {
            "level": LOG_LEVEL,
            "handlers": ["default", "file", "stderr"],
            "propagate": False,
        },
        "uvicorn.access": {
            "handlers": ["uvicorn.access", "file"],
            "level": "INFO",
            "propagate": False,
        },
    },
}


if __name__ == "__main__":
    # Log start of Uvicorn
    logging.info("Starting Uvicorn with Python on %s:%d with log level %s", HOST, PORT, LOG_LEVEL)

    uvicorn.run(
        app,
        host=HOST,
        port=PORT,
        log_level=LOG_LEVEL.lower(),
        log_config=LOGGING_CONFIG, 
        use_colors=True
    )
    
