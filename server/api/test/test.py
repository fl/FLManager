from api.main import app
from fastapi.testclient import TestClient

client = TestClient(app)

def get_access_token():
    response = client.post("/flmanager/API/v1/token", data={"username": "test@test.bsc", "password": "test"})
    assert response.status_code == 200
    return response.json()["access_token"]

def test_list_tools():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    response = client.get("/flmanager/API/v1/tools", headers=headers)
    assert response.status_code == 200
    assert len(response.json()) > 0   

def test_read_tool():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    tool_id = "eucaim-ml-demo"
    response = client.get(f"/flmanager/API/v1/tools/{tool_id}", headers=headers)
    assert response.status_code == 200
    assert "name" in response.json()   

def test_trigger_tool():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    tool_name = "eucaim-ml-demo"
    response = client.get(f"/flmanager/API/v1/tools/job/{tool_name}?nodes=BSC", headers=headers)
    assert response.status_code == 200
    assert "status" in response.json()   

def test_health_check():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    response = client.get("/flmanager/API/v1/hosts/health?nodes=BSC", headers=headers)
    assert response.status_code == 200
    assert "response" in response.json()   

def test_list_user_files():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    user_id = "b6ffac36-6f91-4e2c-a945-85d81f1ced1f"  
    response = client.get(f"/flmanager/API/v1/data/files/{user_id}", headers=headers)
    assert response.status_code == 200
    assert "file_ids" in response.json()

def test_list_execution_files():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    execution_id = "execution123"  
    response = client.get(f"/flmanager/API/v1/data/files/job/{execution_id}", headers=headers)
    assert response.status_code == 200
    assert "files" in response.json()

def test_retrieve_file_access_uri_endpoint():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    file_id = "65e9e9f354027d3d0429b415" 
    response = client.get(f"/flmanager/API/v1/data/file/{file_id}/access", headers=headers)
    assert response.status_code == 200
    assert "access_uri" in response.json()

def test_request_file():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    file_id = "65e9e9f354027d3d0429b415" 
    response = client.get(f"/flmanager/API/v1/data/request_file/{file_id}", headers=headers)
    assert response.status_code == 200
    assert response.json()["status"] == "success"

def test_receive_file():
    access_token = get_access_token()
    headers = {"Authorization": f"Bearer {access_token}"}
    file_id = "file123"  
    files = [("file", ("example.txt", b"file_content"))]  
    response = client.post(f"/flmanager/API/v1/data/push_file_content/{file_id}", files=files, headers=headers)
    assert response.status_code == 200
    assert "processed_files" in response.json()




