# routers/data.py
from fastapi import APIRouter, Depends, HTTPException, Query, UploadFile, File
from services.data_service import *
from services.auth import optional_oauth_token, check_valid_token
from typing import List, Optional
import logging 
from bson import ObjectId
from models.generated_files_request import GeneratedFilesRequest

router = APIRouter()

@router.get("/files/", summary="List File Ids and execution IDs for logged user")
async def list_user_files(user: dict = Depends(optional_oauth_token)):
    """List user files of current user"""
    try:
        check_valid_token(user)
        return list_user_files_service(user.get("user_id"))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list user files: {e}")

@router.get("/files_job/{execution_id}", summary="List of files linked to a specific execution ID")
async def list_execution_files(execution_id: str, user: dict = Depends(optional_oauth_token)):
    """List execution files by execution_id."""
    try:
        check_valid_token(user)
        return list_execution_files_service(execution_id, user.get("user_id"))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list execution files: {e}")
        
@router.get("/list_files_docker", summary="List Files in a Given Path (ONLY INSIDE API DOCKER)")
async def list_files_in_path(
    abs_path: str = Query(..., description="Absolute path to list the files"),
    user: dict = Depends(optional_oauth_token)                          
):
    """
    Endpoint to list files in a given path inside the API docker.
    """
    try:
        check_valid_token(user)

        result = list_files_in_path_service(abs_path)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list files: {e}")

@router.get("/file_content", summary="List File Content (ONLY INSIDE API DOCKER)")
async def list_file_content(
    file_path: str = Query(..., description="The absolute path of the file to list"),
    user: dict = Depends(optional_oauth_token)
):
    """Endpoint to list the content of a test file inside the API dokcer given its path."""
    try:
        # Ensure the user token is valid
        check_valid_token(user)
        
        # Call the service to list the file content
        return list_file_content_service(file_path)

    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list file content: {e}")


@router.put("/file_to_bbdd/{task_id}/executions/{execution_id}", summary="[DEPRECATED] Store Generated Files in DDBB")
async def receive_generated_files(
    task_id:str,
    execution_id: str,
    request_data: GeneratedFilesRequest,
#    credentials: HTTPBasicCredentials = Depends(check_credentials),
    user: dict = Depends(optional_oauth_token),
    #task_id: str = Path(...,description="The task['_id'], not the task['task_id']"),
):
    """Store generated files of a task and execution to the DDBB collection 'executions'"""
    try:
        check_valid_token(user)
#        check_credentials(credentials)
        return await receive_generated_files_service(task_id, user.get("user_id"), execution_id, request_data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to receive generated files: {e}")

# Retrieve the access URI for a file (used to fetch the bytes of the file)
@router.get("/file_uri/{file_id}", summary="Retrieve File Access URI", description="Returns a URI for a given file ID")
def retrieve_file_access_uri(
        file_id: str,
        user: bool = Depends(optional_oauth_token)
):
    ''' Retrieve File Access URI '''
    check_valid_token(user)
    # Check if the file_id is a valid ObjectId
    if not ObjectId.is_valid(file_id):
        raise HTTPException(status_code=400, detail="Invalid file_id. Not a valid ObjectId.")
    return retrieve_file_access_uri_service(file_id)

@router.post("/push_file_content", summary="Push File to orchestrator's docker", description="Receive the file bytes for a given file.")
async def receive_file(
        #sandbox_path: str = Query(...),
        user: dict = Depends(optional_oauth_token),
        execution_id: str = Query(...),
        # credentials: HTTPBasicCredentials = Depends(fedmgr.security),
        files: List[UploadFile] = File(...),
        destination_folder: Optional[str] = Query("tmp_data/"),
):
    ''' Receive File '''
    # Check basic authentication
    # check_credentials(credentials)
    check_valid_token(user)

    logging.info(f"Received request with execution_id: {execution_id}")
    logging.info(f"Files: {[file.filename for file in files]}")
    logging.info(f"Destination folder: {destination_folder}")
    
    if not files:
        logging.error("No files received")
        return {"status": "failure", "message": "No files were uploaded."}

    #return receive_file_service(sandbox_path, user.get("user_id"), execution_id, files, destination_folder)
    logging.info("Calling receive_file_service")
    response = await receive_file_service(execution_id, files, destination_folder, user.get("user_id"))
    logging.info(f"Response from service: {response}")
    return response
