# routers/executions.py
from fastapi import APIRouter, Depends, HTTPException, Query
from services.execution_service import *
from services.auth import optional_oauth_token, check_valid_token
from typing import Optional
import logging

router = APIRouter()

@router.get("/folder_content/{folder_name:path}", summary="Check folder content in a node", description="Check folder content.")
def status_folder(
        folder_name: Optional[str] = "sandbox_path",
        nodes: Optional[str] = Query(None, description="Comma-separated list of nodes for the tool"),
        user: dict = Depends(optional_oauth_token)
):
    ''' Check folder content '''
    check_valid_token(user)
    return status_folder_service(folder_name, nodes, user.get("user_id"), user.get("access_token"))



@router.get("/request_file", summary="Request File by File URI", description="Request the file bytes for a given file URI.")
async def request_file(
        file_uri: str,
        user: dict = Depends(optional_oauth_token)
):
    ''' Request File by File URI '''

    logging.info("Endpoint /request_file called with identifier=%s, file_uri=%s, destination_folder=%s", identifier, file_uri, destination_folder)
    
    try:
        check_valid_token(user)
        return await request_file_service(file_uri=file_uri, user_id=user.get("user_id"), token=user.get("access_token"), identifier=None)
    except Exception as e:
        logging.error("Error in /request_file endpoint: %s", e)
        return {"status": "failure", "message": str(e)}

@router.get("/{execution_id}", summary="Save execution to DDBB")
async def get_execution_status(execution_id: str, user: dict = Depends(optional_oauth_token)):
    """Endpoint to get execution status by ID and store it in the DDBB"""
    try:
        check_valid_token(user)  
        return get_execution_status_service(execution_id)  
    except HTTPException as e:
        raise HTTPException(status_code=e.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to get execution status: {e}")
