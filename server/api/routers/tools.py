# routers/tools.py
from fastapi import APIRouter, Depends, HTTPException, Query
from services.tool_service import *
from services.auth import optional_oauth_token, check_valid_token
from typing import Optional, List

router = APIRouter()

@router.get("/", summary="List Tools", description="Retrieve the list of tools available.")
async def list_tools(user: dict = Depends(optional_oauth_token)):
    """Endpoint to list tools."""
    try:
        check_valid_token(user)
        return list_tools_service()
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list tools: {e}")

@router.get("/{tool_id}", summary="Read Tool", description="Get information about a specific tool.")
async def read_tool(
    tool_id: str,
    user: dict = Depends(optional_oauth_token)
    ):
    """Endpoint to read tool details."""
    try:
        check_valid_token(user)
        return read_tool_service(tool_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to read tool: {e}")

@router.get("/name/{tool_name}", summary="Get Tool ID by Name", description="Retrieve tool ID by tool name.")
async def get_tool_id_by_name(tool_name: str, user: dict = Depends(optional_oauth_token)):
    """Endpoint to get tool ID by tool name."""
    try:
        check_valid_token(user)
        return get_tool_id_by_name_service(tool_name)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to get tool ID: {e}")

@router.get("/job/{tool_name}", summary="Trigger Tool", description="Trigger a specific tool.")
async def trigger_tool(
    tool_name: str,
    nodes: Optional[str] = Query(None, description="Comma-separated list of nodes for the tool"),
    user: dict = Depends(optional_oauth_token)
):
    """Endpoint to trigger tool by name."""
    try:
        check_valid_token(user)  
        user_id = user.get("user_id")
        return trigger_tool_service(tool_name, nodes, user_id, user.get("access_token"))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to trigger tool: {e}")

# Uncomment this endpoint to debug trigger_tool_by_id_service
# @router.get("/trigger_by_id/{tool_id}", summary="Trigger Tool by ID", description="Trigger a specific tool by its ID.")
# async def trigger_tool_by_id(
#     tool_id: str,
#     nodes: Optional[List[str]] = Query(None, description="Comma-separated list of nodes"),
#     user: dict = Depends(optional_oauth_token)
# ):
#     """Endpoint to trigger tool by its ID."""
#     try:
#         check_valid_token(user) 
#         user_id = user.get("user_id")
        
#         return trigger_tool_by_id_service(tool_id, nodes, user_id)
#     except Exception as e:
#         raise HTTPException(status_code=500, detail=f"Failed to trigger tool by ID: {e}")
