# routers/hosts.py
from fastapi import APIRouter, Depends, HTTPException, Query
from services.host_service import *
from services.auth import optional_oauth_token, check_valid_token
from typing import List, Optional
import logging
router = APIRouter()

@router.get("", summary="List Federated Hosts", description="Get information about all federated hosts.")
async def list_hosts(user: dict = Depends(optional_oauth_token)):
    """Endpoint to list hosts."""
    try:
        check_valid_token(user)
        return list_hosts_service()
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list hosts: {e}")

@router.get("/resources", summary="Resources Check of a given client", description="Perform a basic health check on the system.")
async def health_check(
    nodes: Optional[List[str]] = Query(None, description="List of nodes for health check"),
    user: dict = Depends(optional_oauth_token)
):
    """Endpoint to retrieve the resources of a given host"""
    try:
        check_valid_token(user)
        logging.info(f"B, {nodes}, {user.get('user_id')}")
        return health_check_service(nodes, user.get("user_id"), user.get("acess_token"))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to perform health check: {e}")
    
@router.get("/heartbeat", summary="Check nodes and users that are available to RabbitMQ", description="Check list of clients connected to RabbitMQ or if a specific client is alive.")
async def heartbeat(
    node_name: str = None,
    user: dict = Depends(optional_oauth_token)
):
    """Endpoint to check if the nodes are available and have active connections to RabbitMQ"""
    try:
        check_valid_token(user)
        return get_connected_users_rabbitmq_api(node_name)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to retrieve RabbitMQ list of clients: {e}")

@router.get("/{host_id}", summary="Read Host", description="Get information about a specific host.")
async def read_host(host_id: str, user: dict = Depends(optional_oauth_token)):
    """Endpoint to read a specific host."""
    try:
        check_valid_token(user)
        return read_host_service(host_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to read host: {e}")

