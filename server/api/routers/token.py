from fastapi import APIRouter, Depends, HTTPException
from services import fedmgr
from fastapi.security import OAuth2PasswordRequestForm
import logging
from services.auth import DEV_MODE, HARDCODED_USER_ID, HARDCODED_TOKEN

router = APIRouter()

# Token management route
@router.post("", summary="Get Access Token", response_model=dict)
def get_access_token(
    form_data: OAuth2PasswordRequestForm = Depends()
):  
     
    if DEV_MODE:
        logging.info("Demo mode is active. Returning hardcoded token.")
        return {
            "access_token": HARDCODED_TOKEN,
            "token_type": "bearer",
            "user_id": HARDCODED_USER_ID
        }
     
    try:
        return fedmgr.get_access_token(form_data)  # Use FedMgr to handle token requests
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
    
