# routers/tasks.py
from fastapi import APIRouter, Depends, HTTPException
from services.task_service import *
from services.auth import optional_oauth_token, check_valid_token

router = APIRouter()

@router.get("", summary="List Tasks", description="Retrieve a list of tasks.")
async def list_tasks(
    user: dict = Depends(optional_oauth_token)  
):
    try:
        check_valid_token(user)
        return list_tasks_service()
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to list tasks: {e}")

@router.get("/{task_id}", summary="Read Task", description="Get information about specific task")
async def read_task(task_id: str):
    """Endpoint to read tool details."""
    try:
        return read_task_service(task_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Failed to read task: {e}")
