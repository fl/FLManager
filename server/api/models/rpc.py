import uuid
import pika
import logging
import json

class RpcClient:
    '''    RPC Client class    '''
    def __init__(self, parameters):
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True
        )
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.status = False

    def on_response(self, ch, method, props, body):
        ''' Callback function for the response '''
        logging.info(f"On response: {ch}, {method}, body: {body}, props: {props}")
        if self.corr_id == props.correlation_id:
            self.response = body.decode() if body else None
            #if not "error" in (body.decode().lower() if body else ""):
            #    self.status=True

    def call(self, command, node):
        ''' Call the RPC server '''
        self.response = None
        self.corr_id = str(uuid.uuid4())
        logging.info("Publishing command...")
        self.channel.basic_publish(
            exchange='',
            routing_key=node,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=str(command)
        )
        logging.info("Command published. Processing data events...")
        self.connection.process_data_events(time_limit=None)
        logging.info("Finished processing data events.")
        return self.response
