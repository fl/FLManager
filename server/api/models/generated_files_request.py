from pydantic import BaseModel
from typing import List

# Define the request body model
class GeneratedFilesRequest(BaseModel):
    generated_files: List[str]  # List of file paths
    node_name: str

    class Config:
        json_schema_extra = {
            "example": {
                "generated_files": [
                    "/path/to/generated/file1.txt",
                    "/path/to/generated/file2.txt"
                ],
                "node_name": "node1"
            }
        }
