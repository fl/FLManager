''' FedMgr Class to manage global data'''

from threading import Lock
from concurrent.futures import ThreadPoolExecutor
from urllib.parse import urlparse
import logging
import requests
from pymongo import MongoClient
from fastapi import HTTPException
from fastapi.security import HTTPBasic
from services.rpc_service import get_connection_parameters

# Main class ===============================================================================

class FedMgr:
    ''' Class to manage main functionality of the server'''
    def __init__(self, config):
        self.enable_oauth = config.getboolean(
            "OAuth", "enable_oauth", fallback=False
        )
        self.keycloak_url = config["OAuth"]["keycloak_url"]
        self.client_id = config["OAuth"]["client_id"]
        self.client_secret = config["OAuth"]["client_secret"]

        # MongoDB connection
        mongo_connection_string = config["MongoDB"]["mongo_connection_string"]
        mongo_database_name = config.get(
            "MongoDB", "mongo_database_name", fallback=False
        )
        mongo_server = urlparse(mongo_connection_string).hostname

        # Set up MongoDB connection
        client = MongoClient(mongo_connection_string)
        self.mongo_db = client[mongo_database_name]
        self.mongo_collections = {}
        logging.info("MongoDB connected with %s on %s server", mongo_database_name, mongo_server)
        for col in ["data", "hosts", "tools", "tasks", "executions"]:
            self.mongo_collections[col] = self.mongo_db[config["MongoDB"][f"{col}_collection"]]

        self.mongo_lock = Lock()

        # Setup RBMQ
        self.rbmq_parameters = get_connection_parameters(config)
        logging.debug("RabbitMQ parameters: %s", self.rbmq_parameters)

        self.security = HTTPBasic()
        self.executor = ThreadPoolExecutor()

        self.max_retries = config.getint('API', 'max_retries', fallback=5)
        self.retry_delay = config.getint('API', 'retry_delay', fallback=5)

        # Store information about running jobs
        self.running_jobs = {}

        # Store file events
        self.file_events = {}


    def get_access_token(self, form_data):
        ''' Get access token from Keycloak'''
        try:
            token_url = f"{self.keycloak_url}/protocol/openid-connect/token"
            payload = {
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "grant_type": "password",
                "username": form_data.username,
                "password": form_data.password,
                "scope": "openid",
            }
            response = requests.post(token_url, data=payload, timeout=15)
            response_json = response.json()

            if "access_token" not in response_json:
                raise HTTPException(status_code=401, detail="Invalid credentials")

            # Get the user_id from the userinfo endpoint
            userinfo_url = f"{self.keycloak_url}/protocol/openid-connect/userinfo"
            userinfo_response = requests.get(
                userinfo_url,
                headers={"Authorization": f"Bearer {response_json['access_token']}"},
                timeout=15
            )

            if userinfo_response.status_code == 200:
                userinfo_json = userinfo_response.json()
                user_id = userinfo_json.get("sub")
                # user_email = userinfo_json.get("email") # Unused for now
            else:
                user_id = None
            return {"access_token": response_json["access_token"], "user_id": user_id}

        except requests.exceptions.RequestException:
            raise HTTPException(status_code=500, detail="Failed to get access token")