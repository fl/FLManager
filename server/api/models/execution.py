
from services.mongo_service import get_one_mongodb_document
import uuid
import logging

class Execution():
    ''' Class to handle executions '''
    def __init__(self, tool_id=None, user_id=None, execution_id: str = None):
        if execution_id:
            self.id = execution_id
        else:
            self.id = str(uuid.uuid4())
        self.tool_id = tool_id
        self.user_id = user_id
        self.process_info = []
        self.status = None
        self.model = "2.0"
        logging.info("Execution initiated as: %s", self.__dict__)

    def get_from_mongodb(self, collection):
        ''' Get the execution from MongoDB '''
        print("id: ", self.id)
        exec_obj = get_one_mongodb_document(collection, {'_id': self.id})

        if not exec_obj:
            logging.error("Execution %s not found in MongoDB", self.id)
            self.status = 'Not found'
        else:
            self.status = exec_obj.get('status', None)
            self.id = exec_obj.get('id', None)
            self.tool_id = exec_obj.get('tool_id', None)
            self.user_id = exec_obj.get('user_id', None)
            self.process_info = exec_obj.get('process_info', [])
            self.model = exec_obj.get('model', "2.0")

    def save_to_mongodb(self, collection):
        ''' Save the execution to MongoDB '''
        exec_obj = {
            '_id': self.id,
            'id': self.id,
            'tool_id': self.tool_id,
            'user_id': self.user_id,
            'process_info': self.process_info,
            'status': self.status,
            'model': self.model
        }
        logging.debug("Saving execution to MongoDB: %s", exec_obj)
        return collection.insert_one(exec_obj)

    def add_process_info(self, process_info):
        ''' Append task execution information '''
        logging.debug("Adding process info: %s", process_info)
        for process in process_info:
            self.process_info.append(process)
            if not self.status:
                self.status = process['status']
            else:
                if process['status'] == 'failed':
                    self.status = 'failed'

    def get_result(self):
        ''' Get the "result" of the execution in a user-friendly format '''
        return {
            'execution_id': self.id,
            'tool_id': self.tool_id,
            'user_id': self.user_id,
            'process_info': self.process_info,
            'status': self.status,
        }