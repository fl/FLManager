from typing import Union
from pydantic import BaseModel, Field

class HealthCheckResponse(BaseModel):
    status: str = Field(default="success")
    result: dict = Field(...)


class HealthCheckDetails(BaseModel):
    resources_check: Union[str, dict] = Field(default={})
    installation_check: Union[str, dict] = Field(default={})
    gpu_check: Union[str, dict] = Field(default={})
    path_check: Union[str, dict] = Field(default={})
    execution_id: Union[str, dict] = Field(default={})
