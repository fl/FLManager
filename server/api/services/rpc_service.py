""" Support for RPC through RabbitMQ"""

import logging
import ssl
from pathlib import Path
from datetime import datetime
import pika
from models.rpc import RpcClient

def get_connection_parameters(config):
    ''' Get the connection parameters for RabbitMQ '''
    mq_connection = config['RabbitMQ']['mq_connection_string']
    parameters = pika.URLParameters(mq_connection)

    if mq_connection.startswith('amqps'):
        context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        context.check_hostname = False

        cacertfile = Path('certificates/rootCA_cert.pem')
        certfile = Path('certificates/server_cert.pem')
        keyfile = Path('certificates/server_key.pem')

        context.verify_mode = ssl.CERT_NONE

        if cacertfile.exists():
            context.verify_mode = ssl.CERT_REQUIRED
            context.load_verify_locations(cafile=str(cacertfile))

        if certfile.exists():
            assert(keyfile.exists())
            context.load_cert_chain(str(certfile), keyfile=str(keyfile))

        parameters.ssl_options = pika.SSLOptions(context=context, server_hostname=None)
        #parameters.blocked_connection_timeout = 300

    return parameters

def start_call(
        rbmq_parameters,
        execution_id,
        task_id=None,
        tool_id=None,
        nodes=None,
        file_id=None,
        user_id=None,
        token=None,
        folder=None,
):
    ''' Start the RPC call '''
    starting_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    logging.info("Starting time: %s", starting_time)
    try:
        rpc = RpcClient(rbmq_parameters)
    except Exception as e:
        logging.error("Error connecting to RabbitMQ: %s", e)
        return {}

    execution_info = []

    try:
        if folder:
            command = f"{folder}:{execution_id}:{user_id}:{token}"
            logging.info(f" [.] Running command (folder) {command} at {nodes}")
        elif file_id:
            # File request command
            command = f"{file_id}:{execution_id}:{user_id}:{token}"
            logging.info(" [.] Running command (file) %s at %s", command, nodes)
        else:
            # Standard command (task_id, execution_id, and user_id)
            command = f"{tool_id}:{task_id}:{execution_id}:{user_id}:{token}"
            logging.info(" [.] Running command (else) %s", command)

        # Send the command to the specified nodes
        for node in nodes:
            logging.info(f" [.] Running command (loop node) {command} at {node}")
            response = rpc.call(command, node)
            logging.info(f"Response = {response}")

            if response != None:
                logging.info(f" [.] RESPONSE FROM {node}: {response}")
                status = "success"
                return_code = 0
            else:
                logging.info(f" [.] Empty response from {node}")
                status = "failed"
                return_code = 1

            # Register the run in the database
            execution_info.append({
                "execution_id": execution_id,
                "tool_id": command,
                "task_id": task_id,
                "user_id": user_id,
                "node": node,
                "result": response,
                "status": status,
                "return_code": return_code,
                "starting_time": starting_time,
                "finish_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            })
        # Add a log message to indicate the end of processing
        logging.info(f" [x] Finished processing {task_id} on nodes: {nodes}")
        return execution_info

    except Exception as e:
        # Define 'node' here to make it accessible in the except block
        node = "undefined"
        logging.error(f" [!] Exception during execution on node {node}: {e}")
        status = "failed"
        return_code = 1
        response = str(e)
        execution_info.append({
            "execution_id": execution_id,
            "tool_id": command,
            "task_id": task_id,
            "user_id": user_id,
            "result": str(e),
            "status": "failed",
            "return_code": 1,
            "starting_time": starting_time,
            "finish_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        })
        # Add a log message to indicate the end of processing
        logging.error(f" [x] Failed processing {task_id} on nodes: {nodes}")
        return execution_info
