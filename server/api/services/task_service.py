# services/task_service.py
from fastapi import HTTPException
import logging
from services import fedmgr 

def list_tasks_service():
    """Service to list all tasks from MongoDB"""
    try:
        tasks_collection = fedmgr.mongo_collections['tasks']  
        tasks = list(tasks_collection.find())  
        return tasks
    except Exception as e:
        logging.error(f"Error fetching tasks: {e}")
        raise HTTPException(status_code=500, detail=f"Error fetching tasks: {e}")

def read_task_service(task_id):
    # ''' Read a tool by ID '''
    ''' Read a task by ID '''
    try:
        # Fetch tool from tools collection
        task = fedmgr.mongo_collections["tasks"].find_one({"_id": task_id})
        if not task:
            raise HTTPException(status_code=404, detail="Task not found")
        
        return task 
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error fetching task: {e}")
