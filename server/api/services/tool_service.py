# services/tool_service.py
from fastapi import HTTPException
from services import fedmgr
from typing import Optional, List
from models.execution import Execution
import logging
from fastapi.responses import JSONResponse
from services.rpc_service import start_call 

def get_tool_id_by_name_service(tool_name: str):
    """Service to get tool ID by name."""
    try:
        tool = fedmgr.mongo_collections['tools'].find_one({"name": tool_name})
        if not tool:
            raise HTTPException(status_code=404, detail="Tool not found")
        return tool["_id"]
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error fetching tool ID: {e}")

def trigger_tool_by_id_service(
        tool_id: str, 
        node_list: Optional[List[str]], 
        user_id: str,
        token: str,
        ):
    """Service to trigger tool by ID using Execution."""
    # Access fedmgr parameters and MongoDB collections
    rbmq_parameters = fedmgr.rbmq_parameters
    logging.info(f"Rabbit parameters fetched:{rbmq_parameters}")
    
    try:
        tool_info = read_tool_service(tool_id)
        logging.info(f"Creating new Execution instance")

        execution = Execution(tool_id=tool_id, user_id=user_id)
        
        logging.info(f"Created")

        tasks = tool_info.get("tasks", [])
        logging.info(f"[.] Getting tasks associated with {tool_id} execution: {list(tasks)}")

        for task in tasks:
            task_id = task.get("task_id")
            task_nodes = node_list if node_list else task.get("host_list", [])
            logging.info(f"- Preparing task %s at host/s %s", task_id, task_nodes)
            if not task_nodes:
                raise HTTPException(status_code=400, detail=f"No nodes specified and no default nodes in the task (task_id={task_id})")

            # Add execution info for each node
            for task_node in task_nodes:
                logging.info(f"Executing task_id={task_id} on node={task_node}")
                # Add process information, which would typically involve RabbitMQ calls
                execution.add_process_info(
                    start_call(
                        rbmq_parameters=rbmq_parameters,
                        execution_id=execution.id,
                        task_id=task_id,
                        tool_id=tool_id,
                        nodes=[task_node],
                        file_id=None,
                        user_id=user_id,
                        token=token,
                        folder=None
                    )
                )
                logging.info(f"result content: {execution.get_result()}")
                            
            logging.info(f"[.] Tool execution returned")

        # Save execution to MongoDB
        execution.save_to_mongodb(fedmgr.mongo_collections['executions'])
        return execution

    except HTTPException as e:
        logging.error(f"HTTPException: {e}")
        return {"status": "failure", "message": str(e)}
    
def list_tools_service():
    ''' List all tools '''
    # return tools.list_tools(self.mongo_collections['tools'])
    try:
        tools = fedmgr.mongo_collections["tools"].find()
        return list(tools)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error fetching tools: {e}")


def read_tool_service(tool_id):
    # ''' Read a tool by ID '''
    ''' Read a tool by ID, including associated tasks '''
    tools_collection = fedmgr.mongo_collections['tools']
    try:
        tool_info = tools_collection.find_one({"_id": tool_id})
                   
    except Exception as e:
        logging.error(f"Error fetching tool information for tool_id={tool_id}: {e}")
        raise HTTPException(status_code=500, detail="An error occurred while fetching tool information.")
    return tool_info

def trigger_tool_service(tool_name, nodes, user_id, token):
    ''' Trigger a tool execution by name '''
    try:
        logging.info("User %s triggering tool %s at nodes %s", user_id, tool_name, nodes)
        logging.debug("Received tool_name: %s", tool_name)
        tool_id = get_tool_id_by_name_service(
            tool_name,
        )
        logging.info("Tool found with ID %s", tool_id)
        logging.debug("Tool ID: %s", tool_id)

        if tool_id is None:
            raise HTTPException(status_code=404, detail="Tool not found")

        # Split the nodes into a list
        node_list = nodes.split(',') if nodes else None

        execution = trigger_tool_by_id_service(
            tool_id,
            node_list,
            user_id=user_id,
            token=token
        )

        # execution.save_to_mongodb(fedmgr.mongo_collections['executions'])
        # TODO catch Mongodh exception

        logging.info("Registering execution %s for tool %s, user %s", execution.id, tool_id, user_id)
        logging.debug("Execution info: %s", execution.process_info)


        # Create a nice JSON response
        response_content = {"status": "success", "result": execution.get_result()}
        return JSONResponse(content=response_content, status_code=200)


    except HTTPException as e:
        logging.error(e)
        return JSONResponse(content={"status": "failure", "message": str(e)}, status_code=400)
