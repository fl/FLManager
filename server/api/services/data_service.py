# services/data_service.py
from fastapi import HTTPException
import os, uuid
from services import fedmgr
from bson import ObjectId
from services.mongo_service import get_mongodb_documents, insert_one_mongodb_document, get_one_mongodb_document
import logging
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
import asyncio
from datetime import datetime, timezone
from urllib.parse import unquote_plus

def list_user_files_service(user_id: str):
    """Service to list user files."""
    try:
        data_collection = fedmgr.mongo_collections['data']

        # Retrieve the file entries for the specified user_id
        user_files = list(data_collection.find({"user_id": user_id}))
        logging.info(f"User files: {user_files}")

        # Check if any files are found
        if user_files:
            # Extract URIs from file entries
            file_uris = [f"{str(file_entry['_id'])}" for file_entry in user_files]
            exec_ids = [file_entry.get('execution_id') for file_entry in user_files]

            # Return the list of URIs
            return {"user_id": user_id, "file_ids": file_uris, "exec_ids": exec_ids}

        # Return an empty list if no files are found
        return {"No files were found for user_id": user_id, "file_ids": [], "exec_ids": []}

    except Exception as e:
        # Log the error
        logging.error("Error listing files for user_id %s: %s", user_id, e)

        # Return an error response
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": f"Failed to list user files: {str(e)}"
            }),
            status_code=500
        )

def list_execution_files_service(execution_id: str, user_id:str):
    """Service to list execution files."""
    try:
        data_collection = fedmgr.mongo_collections['data']
        # Retrieve the file entries for the specified execution_id
        files_list = get_mongodb_documents(
            data_collection,
            {"execution_id": execution_id,
             "user_id": user_id}
        )

        # Check if any files are found
        if files_list:
            # Convert ObjectId to string for serialization and return the list
            for file in files_list:
                file["_id"] = str(file["_id"])
            return {"execution_id": execution_id, "files": files_list}

        # Return an empty list if no files are found
        return {"execution_id": execution_id, "files": []}

    except Exception as e:
        # Log the error
        logging.error("Error listing files for execution_id %s: %s", execution_id, e)

        # Return an error response
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": f"Failed to list execution files: {str(e)}"
            }),
            status_code=500
        )
def list_files_in_path_service(abs_path: str):
    """Service to list files in a given absolute path."""
    try:
        # Check if the path exists and is a directory
        if not os.path.exists(abs_path):
            return JSONResponse(
                content=jsonable_encoder({
                    "status": "error",
                    "message": f"Path '{abs_path}' does not exist."
                }),
                status_code=404
            )

        if not os.path.isdir(abs_path):
            return JSONResponse(
                content=jsonable_encoder({
                    "status": "error",
                    "message": f"Path '{abs_path}' is not a directory."
                }),
                status_code=400
            )

        # List files in the directory
        files = os.listdir(abs_path)
        
        # Check if there are any files in the directory
        if files:
            return {"status": "success", "files": files}
        else:
            return {"status": "success", "message": "No files found in the specified path."}

    except Exception as e:
        logging.error(f"Error listing files in path '{abs_path}': {e}")
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": f"Failed to list files in path: {str(e)}"
            }),
            status_code=500
        )
    
def list_file_content_service(file_path: str):
    """Service to read the content of a file given its absolute path."""
    try:
        # Check if the file exists
        if not os.path.exists(file_path):
            logging.error(f"File not found: {file_path}")
            raise HTTPException(status_code=404, detail=f"File not found: {file_path}")

        # Check if the path is actually a file
        if not os.path.isfile(file_path):
            logging.error(f"The path is not a file: {file_path}")
            raise HTTPException(status_code=400, detail=f"The path is not a file: {file_path}")

        # Open and read the file
        with open(file_path, 'r') as file:
            file_content = file.read()

        # Return the file content
        return {"file_path": file_path, "content": file_content}

    except Exception as e:
        logging.error(f"Error reading file {file_path}: {e}")
        raise HTTPException(status_code=500, detail=f"Error reading file: {e}")

async def receive_generated_files_service(
        task_id,
        user_id,
        execution_id,
        request_data,
        max_retries=3,
        retry_delay=1
):
    ''' Receive generated files from a task execution '''

    executions_collection = fedmgr.mongo_collections['executions']
    data_collection = fedmgr.mongo_collections['data']
    
    try:
        
        # Check if the task_id is a valid ObjectId
        #if ObjectId.is_valid(task_id):
        #    task_id = ObjectId(task_id)
        if task_id:
            task_id = task_id
        else:
            logging.error("Invalid task_id '%s'. Not a valid ObjectId.", task_id)
            return JSONResponse(
                content=jsonable_encoder({
                    "status": "error",
                    "message": "Invalid task_id. Not a valid ObjectId."
                }),
                status_code=400
            )

        # Extract the 'generated_files' and 'node_name' fields from the request_data
        logging.info(f"request data: {request_data}")
        generated_files = request_data.generated_files
        node_name = request_data.node_name or "Unknown Node"

        # Retry loop for fetching the node if node_name is "Unknown Node"
        attempt = 0

        while node_name == "Unknown Node" and attempt <= max_retries:
            execution = get_one_mongodb_document(
                executions_collection,
                {"_id": execution_id}
            )
            logging.info(f"exution object: {execution}")
            for task_info in execution.process_info:
                node_name = task_info['node']

            if node_name == "Unknown Node":
                attempt += 1
                logging.warning(
                    "Node is 'Unknown Node'. Retrying... (%s/%s)",
                    attempt + 1,
                    max_retries
                )
                await asyncio.sleep(retry_delay)

        if node_name == "Unknown Node":
            logging.error("Node is still 'Unknown Node' after max retries.")
            return JSONResponse(
                content=jsonable_encoder({
                    "status": "error",
                    "message": "Node is 'Unknown Node' after max retries"
                }),
                status_code=500
            )

        # Include the current date and time
        current_date = datetime.now(timezone.utc)

        # Initialize a list to store individual file entries
        file_entries = []

        for file_info in generated_files:
            # Generate a random string for the 'name' field
            random_string = str(uuid.uuid4())
            # Format file_uri as node:file_info
            file_uri = f"{node_name}:{file_info}"

            # Insert each generated file as a separate document with a random 'name'
            result = insert_one_mongodb_document(
                data_collection, {
                    "task_id": task_id,
                    "user_id": user_id,
                    "execution_id": execution_id,
                    "node": node_name,
                    "file_uri": file_uri,
                    "date": current_date,
                    "name": random_string
                }
            )

            # Check if the insertion was successful and store the file entry
            if result.inserted_id:
                file_entries.append({
                    "task_id": task_id,
                    "user_id": user_id,
                    "execution_id": execution_id,
                    "node": node_name,
                    "file_uri": file_uri,
                    "date": current_date,
                    "name": random_string
                })
            else:
                # Log an error if the insertion failed for a file
                logging.error(
                    "Failed to insert generated file for task %s, user %s, execution %s on node %s",
                    task_id, user_id, execution_id, node_name
                )

        if file_entries:
            # Log the successful insertion for all files
            logging.info(
                f"Received and inserted generated files for task %s, user %s, execution %s on node %s at %s",
                task_id, user_id, execution_id, node_name, current_date
            )

            # Return a success response with individual file entries
            return JSONResponse(
                content=jsonable_encoder({
                    "status": "success",
                    "message": "Generated files received and inserted successfully",
                    "file_entries": file_entries
                })
            )

        # Return an error response if no files were successfully inserted
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": "Failed to insert any generated files"
            }),
            status_code=500
        )

    except Exception as e:
        # Log the error
        logging.error(
            "Error processing generated files for task %s, user %s, execution %s: %s",
            task_id, user_id, execution_id, e
        )

        # Return an error response
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": f"Failed to process generated files: {str(e)}"
            }),
            status_code=500
        )

def retrieve_file_access_uri_service(file_id):
    ''' Retrieve the access URI for a file '''
    try:
        data_collection = fedmgr.mongo_collections['data']
        # Retrieve the file entry from the data collection
        file_entry = get_one_mongodb_document(data_collection, {"_id": ObjectId(file_id)})

        # Check if the file entry exists
        if not file_entry:
            raise HTTPException(status_code=404, detail="File entry not found")
        # Convert ObjectId to string for JSON serialization
        file_entry["_id"] = str(file_entry["_id"])

        # Retrieve the generated_files entry from the file_entry
        generated_files = file_entry.get("file_uri", [])

        # Create and return the access URI using the first entry in generated_files
        access_uri = generated_files if generated_files else None

        return {"file_id": file_id, "access_uri": access_uri}

    except Exception as e:
        # Log the error
        logging.error("Error retrieving file access URI for file %s: %s", file_id, e)
        # Return an error response
        return JSONResponse(
            content=jsonable_encoder({
                "status": "error",
                "message": f"Failed to retrieve file access URI: {str(e)}"
            }),
            status_code=500
        )

def get_generated_file_service(identifier=None, file_uri=None):
    ''' Get the generated file info '''
    data_collection = fedmgr.mongo_collections['data']
    node = None
    generated_file = None
    file_info = None
    if file_uri:
        decoded_file_uri = unquote_plus(file_uri)
        if ":" in decoded_file_uri:
            node, generated_file = decoded_file_uri.split(":", 1)
        else:
            file_info = get_one_mongodb_document(
                data_collection,
                {"file_uri": decoded_file_uri}
            )
            node = file_info.get("node", "Unknown")
            generated_file = file_info.get("file_uri")
    elif identifier:
        file_info = get_one_mongodb_document(
            data_collection,
            {"_id": ObjectId(identifier)}
        )
        if file_info:
            node = file_info.get("node", "Unknown")
            generated_file = file_info.get("file_uri")
    else:
        raise HTTPException(status_code=400, detail="Invalid request")
    return node, generated_file, file_info

async def receive_file_service(execution_id, files, destination_folder, user_id):
    ''' Receive a file from a client '''
    try:
        destination_folder = os.path.join(destination_folder, execution_id)
        os.makedirs(destination_folder, exist_ok=True)
        logging.info(f"Directory created or exists: {destination_folder}")

        processed_files = []

        for file in files:
            file_content = await file.read()
            logging.info(f"Received file content for file: {file.filename}. Content Length: {len(file_content)}")

            try:
                # Use the original filename and append _received.txt
                original_filename = file.filename
                received_filename = f"{os.path.splitext(original_filename)[0]}_received.txt"
                file_uri_new = os.path.join(destination_folder, received_filename)
                absolute_path = os.path.abspath(file_uri_new)
                logging.info(f"Absolute path of the file: {absolute_path}")
                
                # Write file content to the specified path
                with open(absolute_path, "wb") as temp_file:
                    temp_file.write(file_content)
                logging.info(f"File {file.filename} written to {absolute_path}")

                # Verify that the file was written successfully
                if os.path.exists(absolute_path):
                    logging.info(f"File exists at path: {absolute_path}")
                else:
                    logging.error(f"File not found at path: {absolute_path} after writing.")
                    raise FileNotFoundError(f"File was not found at path: {absolute_path} after writing.")

                # Include the file information in the database
                current_date = datetime.now(timezone.utc)
                insert_one_mongodb_document(fedmgr.mongo_collections['data'], {
                    "user_id": user_id,
                    "execution_id": execution_id,
                    "file_uri": file_uri_new,
                    "date": current_date,
                    "name": str(uuid.uuid4())
                })
                logging.info(f"File metadata for {file.filename} inserted into MongoDB")

                # TODO check on revealing the path at client side
                # Add processed file details to the list
                processed_files.append({
                    "file_id": received_filename,
                    "filename": file.filename
                })

                # Track event for file processing
                #event_key = f"{file_uri}:{absolute_path}"
                #fedmgr.file_events[event_key] = True
                #logging.info(f"File processing event tracked with key: {event_key}")

            except Exception as e:
                logging.error(
                    "Error processing file content for file %s: %s",
                    file.filename,
                    str(e)
                )
                return {
                    "status": "failure",
                    "message": f"Error processing file {file.filename}: {str(e)}"
                }

        # Return a response with processed files information
        return {
            "message": "All files processed successfully.",
            "processed_files": processed_files
        }

    except asyncio.TimeoutError:
        logging.error("Timeout during file processing.")
        return {"status": "failure", "message": "Timeout during file processing."}

    except Exception as e:
        # Log any other exceptions
        logging.error("Exception: %s", e)
        return {"status": "failure", "message": f"Error processing file content: {str(e)}"}

