from models.fedmanager import FedMgr
import logging, configparser

config = configparser.ConfigParser()
config.read("config.ini")

# Create the FedMgr instance
fedmgr = FedMgr(config)
logging.info("Federation instance created")