""" Utility function to manage MongoDB connection and data insertion. """

from pymongo import MongoClient

# MongoDB connection
def get_mongodb_connection(config):
    ''' Get MongoDB connection '''
    mongo_connection_string = config.get("MongoDB", "mongo_connection_string", fallback="")
    mongo_database_name = config.get("MongoDB", "mongo_database_name", fallback="dt4h-fl")
    # Set up MongoDB connection
    client = MongoClient(mongo_connection_string)
    return client[mongo_database_name]

def get_mongodb_documents(collection, query, projection=None):
    ''' Get documents from MongoDB collection '''
    return list(collection.find(query, projection=projection))

def get_one_mongodb_document(collection, query):
    ''' Get one document from MongoDB collection '''
    return collection.find_one(query)

def insert_one_mongodb_document(collection, document):
    ''' Insert one document into MongoDB collection '''
    return collection.insert_one(document)

def update_one_mongodb_document(collection, query, patch, upsert=False):
    ''' Update one document into MongoDB collection '''
    return collection.update_one(query, {'$set':patch}, upsert=upsert)
