import requests
import logging
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from typing import Optional
import configparser

# Load client_id and client_secret directly from config.ini
config = configparser.ConfigParser()
config.read("config.ini")

client_id = config["OAuth"]["client_id"]
client_secret = config["OAuth"]["client_secret"]
keycloak_url = config["OAuth"]["keycloak_url"]
base_path=config.get("API", "base_path")

# Define the OAuth2 scheme
oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f"{base_path}/token",  
    auto_error=False
)

REQUESTS_TIMEOUT = 30  # Timeout for HTTP requests

# Demo mode flag to bypass Keycloak when down
DEV_MODE = False  # Set this to True for demo mode (bypasses credential checking)
HARDCODED_USER_ID = "demo-user"  # Demo mode user ID
HARDCODED_TOKEN = "dev_token"

def validate_oauth_token(token: Optional[str]) -> dict:

    if DEV_MODE:
        # In demo mode, bypass the token check and return a demo user
        logging.info("Demo mode is active. Skipping token validation.")
        return {"access_token": HARDCODED_TOKEN, "user_id": HARDCODED_USER_ID}
    
    """Validates the OAuth token using Keycloak introspection."""
    if not token:
        raise HTTPException(status_code=401, detail="OAuth token is required")

    try:
        # Prepare the Keycloak introspection endpoint
        introspection_endpoint = f"{keycloak_url}/protocol/openid-connect/token/introspect"
        introspection_payload = {
            "token": token,
            "client_id": client_id,  # Loaded from config.ini
            "client_secret": client_secret,  # Loaded from config.ini
        }

        logging.debug(f"Validating token at: {introspection_endpoint}")

        # Send the introspection request
        introspection_response = requests.post(
            introspection_endpoint, data=introspection_payload, timeout=REQUESTS_TIMEOUT
        )
        introspection_json = introspection_response.json()

        # Check if the token is active (valid)
        if not introspection_json.get("active"):
            raise HTTPException(status_code=401, detail="Invalid authentication token")

        # Return token information
        return {"access_token": token, "user_id": introspection_json.get("sub")}

    except requests.exceptions.RequestException as e:
        logging.error(f"Token validation failed: {e}")
        raise HTTPException(status_code=500, detail="Failed to validate authentication token")

# Optional OAuth token dependency
async def optional_oauth_token(token: str = Depends(oauth2_scheme)) -> dict:
    """Obtain and optionally validate an OAuth token."""
    try:
        # Validate the token using Keycloak introspection
        oauth_info = validate_oauth_token(token)
        return {"access_token": token, "user_id": oauth_info.get("user_id")}
    except HTTPException as e:
        raise e

def check_valid_token(oauth_token: dict):
    # Add logic to verify token (replace with actual logic)
    if not oauth_token:
        raise HTTPException(status_code=401, detail="Invalid token")
