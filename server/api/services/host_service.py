# services/host_service.py
import requests
from fastapi import HTTPException
from services import fedmgr
from typing import List
import logging
from models.health import *
import json
from json.decoder import JSONDecodeError
import configparser
from requests.auth import HTTPBasicAuth
from fastapi.responses import JSONResponse
from services.tool_service import get_tool_id_by_name_service, trigger_tool_by_id_service

#Some function to help parsing the resources JSON result
def try_parse_json(s):
    ''' Try to parse JSON '''
    try:
        return json.loads(s)
    except JSONDecodeError:
        return s
    
def clean_string(s):
    ''' Clean string '''
    return s.replace("\n", "").replace("'", "\"").strip("'\"")

# Function to parse health check results (previously in health.py)
def parse_health_check_result(result_dict, execution_id):
    ''' Extract relevant information from the response '''
    response_parts = result_dict['response'].split("\n\n")

    if len(response_parts) > 3:
        resources_check = try_parse_json(clean_string(response_parts[3]))
    else:
        resources_check = {}

    if len(response_parts) > 1:
        gpu_check = try_parse_json(clean_string(response_parts[1]))
    else:
        gpu_check = {}

    if len(response_parts) > 2:
        path_check = try_parse_json(clean_string(response_parts[2]))
    else:
        path_check = {}

    if response_parts:
        installation_check = try_parse_json(clean_string(response_parts[0]))
    else:
        installation_check = {}

    return HealthCheckDetails(
        resources_check=resources_check,
        installation_check=installation_check,
        gpu_check=gpu_check,
        path_check=path_check,
        execution_id=execution_id
    )

# Service to list federated hosts
def list_hosts_service():
    """Service to list all federated hosts using MongoDB from fedmgr."""
    try:
        hosts_collection = fedmgr.mongo_collections['hosts']  # Access the hosts collection
        hosts = list(hosts_collection.find())  # Fetch hosts from MongoDB
        return hosts
    except Exception as e:
        logging.info(f"Database error: {e}")
        raise HTTPException(status_code=500, detail="An error occurred while fetching host information.")

# Service to read a specific host by ID
def read_host_service(host_id: str):
    """Service to read a specific host by ID using MongoDB from fedmgr."""
    try:
        hosts_collection = fedmgr.mongo_collections['hosts']
        host = hosts_collection.find_one({"_id": host_id})
        if not host:
            raise HTTPException(status_code=404, detail="Federated host not found")
        return host
    except Exception as e:
        logging.info(f"Database error: {e}")
        raise HTTPException(status_code=500, detail="An error occurred while fetching host information.")

# Service to perform a health check on nodes
def health_check_service(nodes: List[str], user_id:str, token:str):
    """Service to perform a health check on the system."""

    try:
        tool_id = get_tool_id_by_name_service("Health Check")
        logging.info(f"Tool ID and user ID: {tool_id}, {user_id}")
        if tool_id is None:
            raise HTTPException(status_code=404, detail="Tool not found")

    except HTTPException as e:
        return {"status": "failure fetching tool", "message": str(e)}

    try:
        execution =trigger_tool_by_id_service(
                tool_id,
                nodes,
                user_id=user_id,
                token=token
            )

        logging.info(f"execution content: {execution.get_result()}")
        health_check_details = {}
        for value in execution.process_info:
            health_check_details[value['node']] = parse_health_check_result(json.loads(value['result']), execution.id).model_dump()

        logging.info("Health check details: %s", health_check_details)

        # Return a JSONResponse with the parsed data
        return JSONResponse(content=health_check_details, status_code=200)

    except HTTPException as e:
        logging.info(e)
        return {"status": "failure", "message": str(e)}
    except Exception as e:
        logging.info(e)
        return {"status": "failure", "message": "An unexpected error occurred"}
    
def get_connected_users_rabbitmq_api(node_name=None):
    ''' Fetch and display list of connected users using RabbitMQ Management API '''
    config = configparser.ConfigParser()
    config.read("config.ini")  

    # Read RabbitMQ credentials from the config.ini
    rabbitmq_base_url = config.get("RabbitMQ", "rabbitmq_base_url", fallback="")  

    url_connections = f"{rabbitmq_base_url}connections"
    url_queues = f"{rabbitmq_base_url}queues"
    username = config.get("RabbitMQ", "rabbitmq_user", fallback="")
    password = config.get("RabbitMQ", "rabbitmq_password", fallback="")

    try:

        # Fetch queue data to map queue names
        response_queues = requests.get(
            url_queues,
            auth=HTTPBasicAuth(username, password),
            verify=False
        )

        if response_queues.status_code == 200:
            queues = response_queues.json()

            if not queues:
                logging.info("No active queues found.")
                return []

            # Create a dictionary to map nodes to their respective queue names
            node_to_queue_name = {queue['node']: queue['name'] for queue in queues}

        else:
            logging.error(f"Failed to access RabbitMQ Queues API. Status Code: {response_queues.status_code}")
            return []

        #Fetch connections data
        response_connections = requests.get(
            url_connections,
            auth=HTTPBasicAuth(username, password),
            verify=False  # Disable SSL certificate verification
        )

        if response_connections.status_code == 200:
            connections = response_connections.json()

            if not connections:
                logging.info("No active connections found.")
                return []

            users_info = []
            for connection in connections:
                user = connection.get("user")
                node = connection.get("node")
                client_host = connection.get("peer_host")
                client_port = connection.get("peer_port")
                state = connection.get("state")

                # Use the queue name if available, otherwise default to the node name
                queue_name = node_to_queue_name.get(node, node)

                # If a specific node_name is passed, filter by that queue name
                if node_name and queue_name != node_name:
                    continue  # Skip connections that do not match the specified node

                users_info.append({
                    "user": user,
                    "node": queue_name,
                    "host": client_host,
                    "port": client_port,
                    "state": state
                })

            # Print the connected users
            for user_info in users_info:
                logging.info(f"User: {user_info['user']}, Node: {user_info['node']}, "
                             f"Host: {user_info['host']}, Port: {user_info['port']}, "
                             f"State: {user_info['state']}")

            return users_info

        else:
            logging.error(f"Failed to access RabbitMQ API. Status Code: {response_connections.status_code}")
            logging.error(f"Response Text: {response_connections.text}")
            return []

    except requests.exceptions.SSLError as ssl_error:
        logging.error(f"SSL Error: {ssl_error}")
    except Exception as e:
        logging.error(f"Error: {e}")

    return []
