# services/execution_service.py
import os
from fastapi import HTTPException
from fastapi.responses import FileResponse
from services import fedmgr
from services.data_service import get_generated_file_service
import logging 
from urllib.parse import unquote_plus
from services.rpc_service import start_call
import asyncio
from datetime import datetime, timedelta
from models.execution import Execution


def get_execution_status_service( execution_id: str):
    """Service to get execution status by execution_id."""
    if not execution_id:
        raise HTTPException(status_code=400, detail="Execution ID is required")
    try:
        execution = Execution(execution_id=execution_id)
        execution.get_from_mongodb(fedmgr.mongo_collections['executions'])
        if execution.status == 'Not found':
            raise HTTPException(status_code=404, detail="Execution not found")
        return {
        "status": execution.status,
        "result": execution.get_result(),
    }
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error fetching execution status: {e}")
    
def status_folder_service(folder_name, nodes, user_id, token):
        ''' Get the contents of a remote folder '''
        try:
            # If folder_name is not provided, use "default"
            folder_name = "sandbox_path" if not folder_name else unquote_plus(folder_name)

            logging.info("Received folder_name: %s", folder_name)

            # Split the nodes into a list
            node_list = nodes.split(',') if nodes else None

            execution = Execution(user_id=user_id)
            execution.add_process_info(
                start_call(
                    fedmgr.rbmq_parameters,
                    execution.id,
                    folder=folder_name,
                    nodes=node_list,
                    user_id=user_id,
                    token=token
                )
            )

            execution.save_to_mongodb(fedmgr.mongo_collections['executions'])
            logging.info("Execution saved to MongoDB with ID: %s", execution.id)

            # Log when waiting is completed
            logging.info(f"Request completed for folder: {folder_name}. Response: {execution.status}")

            return {"message": "Folder content retrieved successfully.", "folder_content": execution.get_result()}
        except HTTPException as e:
            # Log any HTTPException that occurs
            logging.error(f"HTTPException: {e}")
            return {"status": "failure", "message": str(e)}
        except Exception as ex:
            # Log any other exceptions
            logging.error(f"Exception: {ex}")
            return {"status": "failure", "message": str(ex)}
        
async def request_file_service(identifier, file_uri, user_id, token):
        ''' Request a file from a remote node '''
        try:
            logging.info("Received request for identifier: %s or %s", identifier, file_uri)
            node, generated_file, file_info = get_generated_file_service(
                # self.mongo_collections['data'],
                identifier=identifier,
                file_uri=file_uri
            )
            logging.info(f"Node: {node}, generated_file: {generated_file}, file_info: {file_info}")

            if not node or not generated_file:
                return {
                    "status": "failure",
                    "detail": "File not found or node information is missing."
                }

            user_id_db = file_info.get("user_id", None) if file_info else None

            if user_id_db and user_id != user_id_db:
                return {
                    "status": "failure",
                    "detail": "File doesn't belong to the requester. Retrieval aborted."
                }

            combined_file_id = f"{generated_file}"

            logging.info("Retrieval initiated for file: %s at node %s", generated_file, node)

            execution_id = file_uri.split('/')[1]

            execution = Execution(execution_id=execution_id, user_id=user_id)
            execution.add_process_info(
                start_call(
                    fedmgr.rbmq_parameters,
                    execution.id,
                    file_id=generated_file,
                    nodes=[node],
                    user_id=user_id,
                    token=token
                )
            )

            logging.debug(execution)
            logging.info(
                "Initiating file retrieval process for identifier: %s, node: %s, execution: %s, user: %s",
                identifier or file_uri, node, execution.id, user_id
            )

            #execution.save_to_mongodb(fedmgr.mongo_collections['executions'])

            logging.info("File request completed for file: %s", generated_file)

            #timeout_duration = 10
            #start_time = datetime.now()

            #file_path = None
            #logging.info(f"fedmgr file events: {fedmgr.file_events}")
            #while datetime.now() - start_time < timedelta(seconds=timeout_duration):
            #    logging.info("Checking for file uri %s", combined_file_id)
            #    for key in fedmgr.file_events:
            #        if key.startswith(combined_file_id):
            #            file_path = key.split(":", 1)[1]
            #            logging.info("File received. %s", fedmgr.file_events)
            #            break
            #    if file_path:
            #        break
            #    await asyncio.sleep(1)
            #else:
            #    logging.info("Timeout: File not received within the specified time.")
            file_name = generated_file.split("/")[2].replace('.txt', '_received.txt')
            file_path = f"/api/tmp_data/{execution_id}/{file_name}"
            
            logging.info(f"Looking for file at path: {file_path}")

            # Check if the file exists
            if os.path.exists(file_path):
                logging.info(f"File exists. Returning file from path: {file_path}")
                return FileResponse(
                    file_path,
                    media_type='application/octet-stream',
                    filename=os.path.basename(file_path)
                )
            else:
                logging.error(f"File not found at path: {file_path}")
                return {"status": "failure", "detail": "File not found in the specified destination folder."}


        except HTTPException as e:
            logging.error(f"HTTPException: {e}")
            return {"status": "failure", "message": str(e)}
        except Exception as ex:
            logging.error(f"Exception: {ex}")
            return {"status": "failure", "message": str(ex)}
