from pymongo import MongoClient, errors

def get_mongo_connection(connection_string):
    client = MongoClient(connection_string)
    return client

def create_collection(db, collection_name):
    return db[collection_name]


def create_index(collection, index_name):
    try:
        collection.create_index(index_name)
    except errors.DuplicateKeyError:
        print(f"Index '{index_name}' already exists in collection '{collection.name}'.")

def create_unique_index(collection, index_name):
    try:
        collection.create_index(index_name, unique=True)
    except errors.DuplicateKeyError:
        print(f"Unique index '{index_name}' already exists in collection '{collection.name}'.")

def create_executions_collection(db):
    try:
        executions = create_collection(db, 'executions')
        create_index(executions, 'job_id')
        print("Collection 'executions' created successfully.")
    except Exception as e:
        print(f"Error creating collection: {e}")

def create_hosts_collection(db):
    try:
        hosts = create_collection(db, 'hosts')
        create_index(hosts, 'name')  # Assuming 'name' should be unique
        print("Collection 'hosts' created successfully.")
    except Exception as e:
        print(f"Error creating collection: {e}")

def create_tasks_collection(db):
    try:
        tasks = create_collection(db, 'tasks')
        create_index(tasks, 'id')
        create_unique_index(tasks, 'name')  # Assuming 'name' should be unique
        print("Collection 'tasks' created successfully.")
    except Exception as e:
        print(f"Error creating collection: {e}")

def create_tools_collection(db):
    try:
        tools = create_collection(db, 'tools')
        create_index(tools, 'id')
        create_unique_index(tools, 'name')  # Assuming 'name' should be unique
        print("Collection 'tools' created successfully.")
    except Exception as e:
        print(f"Error creating collection: {e}")

def create_data_collection(db):
    try:
        data = create_collection(db, 'data')
        create_index(data, 'id')
        create_unique_index(data, 'name')  # Assuming 'name' should be unique
        print("Collection 'data' created successfully.")
    except Exception as e:
        print(f"Error creating collection: {e}")

if __name__ == '__main__':
    try:
        # Replace the connection string and database name with your actual values
        connection_string = 'mongodb://root:q2D6ghQZgpqUVaJF@localhost:27017/'
        db_name = 'dt4h-fl'

        mongo_client = get_mongo_connection(connection_string)
        mongo_db = mongo_client[db_name]

        create_executions_collection(mongo_db)
        create_hosts_collection(mongo_db)
        create_tasks_collection(mongo_db)
        create_tools_collection(mongo_db)
        create_data_collection(mongo_db)

        mongo_client.close()
    except Exception as e:
        print(f"Error: {e}")

