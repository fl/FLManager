from pymongo import MongoClient

def change_password(username, new_password):
    # MongoDB connection string with the old password
    old_connection_string = 'mongodb://root:pass123@localhost:27017/'

    # Connect to MongoDB with the old password
    client = MongoClient(old_connection_string)
    admin_db = client['admin']

    try:
        # Change the user's password
        admin_db.command('updateUser', username, pwd=new_password)
        print(f"Password for user '{username}' updated successfully.")
    except Exception as e:
        print(f"Error changing password: {e}")
    finally:
        # Close the connection
        client.close()

def create_collection(db, collection_name):
    try:
        collection = db.create_collection(collection_name)
        return collection
    except Exception as e:
        print(f"Error creating collection: {e}")
        return None

def create_data_collection(db_name='your_database_name'):
    try:
        # MongoDB connection string
        connection_string = 'mongodb://root:pass123@localhost:27017/'

        # Connect to MongoDB
        client = MongoClient(connection_string)
        db = client[db_name]

        # Create 'data' collection
        data_collection = create_collection(db, 'data')

        # Add additional index creation for the 'data' collection if needed
        print("Collection 'data' created successfully.")
        
        return data_collection
    except Exception as e:
        print(f"Error creating collection: {e}")
    finally:
        # Close the connection
        client.close()

if __name__ == '__main__':
    create_data_collection(db_name='FLdb')

